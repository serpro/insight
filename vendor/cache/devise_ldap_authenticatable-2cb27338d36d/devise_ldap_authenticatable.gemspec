# -*- encoding: utf-8 -*-
# stub: devise_ldap_authenticatable 0.8.3 ruby lib

Gem::Specification.new do |s|
  s.name = "devise_ldap_authenticatable"
  s.version = "0.8.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Curtis Schiewek", "Daniel McNevin", "Steven Xu"]
  s.date = "2016-05-20"
  s.description = "Devise extension to allow authentication via LDAP"
  s.email = "curtis.schiewek@gmail.com"
  s.files = [".gitignore", "CHANGELOG.md", "Gemfile", "MIT-LICENSE", "README.md", "Rakefile", "devise_ldap_authenticatable.gemspec", "lib/devise_ldap_authenticatable.rb", "lib/devise_ldap_authenticatable/exception.rb", "lib/devise_ldap_authenticatable/ldap/adapter.rb", "lib/devise_ldap_authenticatable/ldap/connection.rb", "lib/devise_ldap_authenticatable/logger.rb", "lib/devise_ldap_authenticatable/model.rb", "lib/devise_ldap_authenticatable/strategy.rb", "lib/devise_ldap_authenticatable/version.rb", "lib/generators/devise_ldap_authenticatable/install_generator.rb", "lib/generators/devise_ldap_authenticatable/templates/ldap.yml", "spec/ldap/.gitignore", "spec/ldap/base.ldif", "spec/ldap/clear.ldif", "spec/ldap/local.schema", "spec/ldap/openldap-data/.gitignore", "spec/ldap/openldap-data/run/.gitignore", "spec/ldap/openldap-data/run/.gitkeep", "spec/ldap/run-server", "spec/ldap/server.pem", "spec/ldap/slapd-test.conf.erb", "spec/rails_app/Rakefile", "spec/rails_app/app/controllers/application_controller.rb", "spec/rails_app/app/controllers/posts_controller.rb", "spec/rails_app/app/helpers/application_helper.rb", "spec/rails_app/app/helpers/posts_helper.rb", "spec/rails_app/app/models/post.rb", "spec/rails_app/app/models/user.rb", "spec/rails_app/app/views/layouts/application.html.erb", "spec/rails_app/app/views/posts/index.html.erb", "spec/rails_app/config.ru", "spec/rails_app/config/application.rb", "spec/rails_app/config/boot.rb", "spec/rails_app/config/cucumber.yml", "spec/rails_app/config/database.yml", "spec/rails_app/config/environment.rb", "spec/rails_app/config/environments/development.rb", "spec/rails_app/config/environments/production.rb", "spec/rails_app/config/environments/test.rb", "spec/rails_app/config/initializers/backtrace_silencers.rb", "spec/rails_app/config/initializers/devise.rb", "spec/rails_app/config/initializers/inflections.rb", "spec/rails_app/config/initializers/mime_types.rb", "spec/rails_app/config/initializers/secret_token.rb", "spec/rails_app/config/initializers/session_store.rb", "spec/rails_app/config/ldap.yml", "spec/rails_app/config/ldap_with_boolean_ssl.yml", "spec/rails_app/config/ldap_with_erb.yml", "spec/rails_app/config/ldap_with_uid.yml", "spec/rails_app/config/locales/devise.en.yml", "spec/rails_app/config/locales/en.yml", "spec/rails_app/config/routes.rb", "spec/rails_app/config/ssl_ldap.yml", "spec/rails_app/config/ssl_ldap_with_erb.yml", "spec/rails_app/config/ssl_ldap_with_uid.yml", "spec/rails_app/db/migrate/20100708120448_devise_create_users.rb", "spec/rails_app/db/schema.rb", "spec/rails_app/features/manage_logins.feature", "spec/rails_app/features/step_definitions/login_steps.rb", "spec/rails_app/features/step_definitions/web_steps.rb", "spec/rails_app/features/support/env.rb", "spec/rails_app/features/support/paths.rb", "spec/rails_app/lib/tasks/.gitkeep", "spec/rails_app/lib/tasks/cucumber.rake", "spec/rails_app/public/404.html", "spec/rails_app/public/422.html", "spec/rails_app/public/500.html", "spec/rails_app/public/images/rails.png", "spec/rails_app/public/javascripts/application.js", "spec/rails_app/public/javascripts/controls.js", "spec/rails_app/public/javascripts/dragdrop.js", "spec/rails_app/public/javascripts/effects.js", "spec/rails_app/public/javascripts/prototype.js", "spec/rails_app/public/javascripts/rails.js", "spec/rails_app/public/stylesheets/.gitkeep", "spec/rails_app/script/cucumber", "spec/rails_app/script/rails", "spec/spec_helper.rb", "spec/support/factories.rb", "spec/unit/connection_spec.rb", "spec/unit/user_spec.rb"]
  s.homepage = "https://github.com/cschiewek/devise_ldap_authenticatable"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.8"
  s.summary = "Devise extension to allow authentication via LDAP"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<devise>, [">= 3.0"])
      s.add_runtime_dependency(%q<net-ldap>, ["< 0.6.0", ">= 0.3.1"])
      s.add_development_dependency(%q<rake>, [">= 0.9"])
      s.add_development_dependency(%q<rdoc>, [">= 3"])
      s.add_development_dependency(%q<rails>, [">= 4.0"])
      s.add_development_dependency(%q<sqlite3>, [">= 0"])
      s.add_development_dependency(%q<factory_girl_rails>, ["~> 1.0"])
      s.add_development_dependency(%q<factory_girl>, ["~> 2.0"])
      s.add_development_dependency(%q<rspec-rails>, [">= 0"])
      s.add_development_dependency(%q<database_cleaner>, [">= 0"])
      s.add_development_dependency(%q<capybara>, [">= 0"])
      s.add_development_dependency(%q<launchy>, [">= 0"])
    else
      s.add_dependency(%q<devise>, [">= 3.0"])
      s.add_dependency(%q<net-ldap>, ["< 0.6.0", ">= 0.3.1"])
      s.add_dependency(%q<rake>, [">= 0.9"])
      s.add_dependency(%q<rdoc>, [">= 3"])
      s.add_dependency(%q<rails>, [">= 4.0"])
      s.add_dependency(%q<sqlite3>, [">= 0"])
      s.add_dependency(%q<factory_girl_rails>, ["~> 1.0"])
      s.add_dependency(%q<factory_girl>, ["~> 2.0"])
      s.add_dependency(%q<rspec-rails>, [">= 0"])
      s.add_dependency(%q<database_cleaner>, [">= 0"])
      s.add_dependency(%q<capybara>, [">= 0"])
      s.add_dependency(%q<launchy>, [">= 0"])
    end
  else
    s.add_dependency(%q<devise>, [">= 3.0"])
    s.add_dependency(%q<net-ldap>, ["< 0.6.0", ">= 0.3.1"])
    s.add_dependency(%q<rake>, [">= 0.9"])
    s.add_dependency(%q<rdoc>, [">= 3"])
    s.add_dependency(%q<rails>, [">= 4.0"])
    s.add_dependency(%q<sqlite3>, [">= 0"])
    s.add_dependency(%q<factory_girl_rails>, ["~> 1.0"])
    s.add_dependency(%q<factory_girl>, ["~> 2.0"])
    s.add_dependency(%q<rspec-rails>, [">= 0"])
    s.add_dependency(%q<database_cleaner>, [">= 0"])
    s.add_dependency(%q<capybara>, [">= 0"])
    s.add_dependency(%q<launchy>, [">= 0"])
  end
end
