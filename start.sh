#!/bin/bash
# SCRIPT DE INICIALIZAÇÃO DO FEEDBACK
# DEVE ser chamado este script com as variável de ambiente FEEDBACK_CONF_PATH e FEEDBACK_APP_PATH sendo definida s
#
# exemplo: 
#
#         FEEDBACK_CONF_PATH=/opt/appconf/feedback/  /opt/websites/feedback/start.sh
#      
echo "Starting Feedback"

echo "FEEDBACK_CONF_PATH=$FEEDBACK_CONF_PATH"

if [ ! -d $FEEDBACK_CONF_PATH ]; then
    echo "FEEDBACK_CONF_PATH não definido ou diretório $FEEDBACK_CONF_PATH não encontrado";
    exit 1;
fi



# default variable
export FEEDBACK_UNICORN_BIND_ADDRESS=127.0.0.1
export FEEDBACK_UNICORN_PID=feedback_unicorn.pid
export FEEDBACK_UNICORN_PORT=5040
export FEEDBACK_UNICORN_WORKER_PROCESSES=1

# ambiente.sh deve sobrescrever  exportando as  seguintes variáveis FEEDBACK_UNICORN_PID, FEEDBACK_UNICORN_PORT, FEEDBACK_UNICORN_WORKER_PROCESSES

# precisa da variavel SECRET_KEY_BASE definida

if [ -f $FEEDBACK_CONF_PATH/ambiente.sh ]; then
    source $FEEDBACK_CONF_PATH/ambiente.sh
fi

unicorn -c $FEEDBACK_CONF_PATH/unicorn.rb --daemonize
