# http://michaelvanrooijen.com/articles/2011/06/01-more-concurrency-on-a-single-heroku-dyno-with-the-new-celadon-cedar-stack/

worker_processes_value = 1
worker_processes_value  = ENV["FEEDBACK_UNICORN_WORKER_PROCESSES"].to_i unless ENV["FEEDBACK_WORKER_PROCESSES"] 


worker_processes worker_processes_value  # amount of unicorn workers to spin up

timeout 30         # restarts workers that hang for 30 seconds

preload_app true

listen "#{ENV['FEEDBACK_UNICORN_BIND_ADDRESS']}:#{ENV['FEEDBACK_UNICORN_PORT']}"

pid ENV['FEEDBACK_UNICORN_PID']

# Taken from github: https://github.com/blog/517-unicorn
# Though everyone uses pretty miuch the same code
before_fork do |server, _worker|
  ##
  # When sent a USR2, Unicorn will suffix its pidfile with .oldbin and
  # immediately start loading up a new version of itself (loaded with a new
  # version of our app). When this new Unicorn is completely loaded
  # it will begin spawning workers. The first worker spawned will check to
  # see if an .oldbin pidfile exists. If so, this means we've just booted up
  # a new Unicorn and need to tell the old one that it can now die. To do so
  # we send it a QUIT.
  #
  # Using this method we get 0 downtime deploys.
  ActiveRecord::Base.connection.disconnect! if defined?(ActiveRecord::Base)


  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exist?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      warn "Unicorn: master process already killed, no problem"
    end
  end
end


after_fork do |server, worker|
  ActiveRecord::Base.establish_connection if defined?(ActiveRecord::Base)
end

