#!/bin/bash
# SCRIPT DE PARADA  DO FEEDBACK
# DEVE ser chamado este script com as variável de ambiente FEEDBACK_CONF_PATH e FEEDBACK_APP_PATH sendo definida s
#
# exemplo: 
#
#         FEEDBACK_CONF_PATH=/opt/appconf/feedback/  /opt/websites/errbit/start.sh
#      
echo "Parando o  FEEDBACK"

export FEEDBACK_UNICORN_PID=feedback_unicorn.pid


if [ -f $FEEDBACK_CONF_PATH/ambiente.sh ]; then
    source $FEEDBACK_CONF_PATH/ambiente.sh
fi


pid_number=$(cat $FEEDBACK_UNICORN_PID)

echo "PARANDO O feedback - Processo:  $pid_number"

kill -s QUIT "$pid_number"

